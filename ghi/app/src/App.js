import logo from './logo.svg';
import './App.css';
import Nav from './components/Nav';
import AttendeesList from './components/AttendeesList';
import LocationForm from './components/LocationForm';
import ConferenceForm from './components/ConferenceForm';
import AttendForm from './components/AttendConference';
import PresentationForm from './components/PresentationForm';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import MainPage from './components/MainPage';



function App() {
  return (
    
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index={true} element={<MainPage />}/>
        <Route path='/conferences/new/' element={<ConferenceForm />}/>
        <Route path='attendees'>
          <Route index element={<AttendeesList />}/>
          <Route path='/attendees/new/' element={<AttendForm />}/>
        </Route>
        <Route path='/locations/new/' element={<LocationForm />}/>
        <Route path='/attendees' element={<AttendeesList />}/>
        <Route path='/presentations/new/' element={<PresentationForm />}/>
      </Routes>
    </BrowserRouter>
    
  );
}

export default App;
