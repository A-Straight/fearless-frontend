import { useState, useEffect } from 'react';

function AttendeesList () {
    const [attendees, setAttendees] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8001/api/attendees/');
        if (response.ok){
            const data = await response.json();
            setAttendees(data.attendees);
        }
    }
    useEffect(()=> {
        getData();
    }, [])

    const handleDelete = async (id) => {
        const request = await fetch(`http://localhost:8001/api/attendees/${id}/`, { method: "DELETE" });
        if (request.ok){
            getData();
        }
    }
    
    return (
        <>
        <table className='table table-striped table-hover'>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Conference</th>
                </tr>
            </thead>
            <tbody>
                {attendees.map(attendee => {
                    return (
                        <tr key={attendee.href}>
                            <td>{ attendee.name }</td>
                            <td>{ attendee.conference }</td>
                            <td><button onClick={()=> {
                                handleDelete(attendee.id)
                            }} className='btn btn-danger'>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}

export default AttendeesList;

// async function loadAttendees(){
//   const response = await fetch('http://localhost:8001/api/attendees/');
//   if (response.ok) {
//     const data = await response.json();
//     root.render(
//       <React.StrictMode>
//         <App attendees={data.attendees} />
//       </React.StrictMode>
//     )
//   } else {
//     console.error(response);
//   }
// }
// loadAttendees();
